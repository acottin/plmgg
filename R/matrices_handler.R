#' Recombination function
#'
#' Take two haplotype and do a crossing over, generating a random break point
#' @param hap1 haplotype 1 (as a vector)
#' @param hap2 haplotype 2 (as a vector)
#' @param pos_recomb breakpoint position for recombination
#'
#' @return vector newly produced haplotype
#' @export
#'
#' @examples
#' recomb_hap( hap1 = mat_hap[5,],
#'            hap2 = mat_hap[6,])
recomb_hap <- function(hap1,
                      hap2,
                      pos_recomb) {
  # Randomly pick a site (SNP position) where the crossing over happen
  # Concatenate haplotype h1 chunk (from the beginning to the crossing over
  # position) with haplotype h2 chunk
  new_hap <- c(hap1[1:pos_recomb],
              hap2[(pos_recomb + 1):length(hap2)])
  return(new_hap)
}

#' Breeding function
#'
#' Take 4 haplotypes (2 chrom) sequences and origins and recombine it
#' @param haps1 matrix of 2 rows = two haplotypes
#' @param haps2 matrix of 2 rows = two haplotypes
#' @param oris1 matrix of 2 rows = two haplotypes origin
#' @param oris2 matrix of 2 rows = two haplotypes origin
#' @export
#'
#' @return list $hap with the new hap and $ori with the new hap origin
breed_hap <- function(haps1,
                      haps2,
                      oris1,
                      oris2) {

  #get recombination position
  p_recomb <- sample(2:(ncol(haps1) - 1), 2, replace = TRUE)
  #choose haplotype order
  hap_order1 <- sample(2, 2, replace = FALSE)
  hap_order2 <- sample(2, 2, replace = FALSE)
  #do recombination
  hap_new1 <- recomb_hap(hap1 = haps1[hap_order1[1], ],
                         hap2 = haps1[hap_order1[2], ],
                         pos_recomb = p_recomb[1])
  #same for origins
  ori_new1 <- recomb_hap(hap1 = oris1[hap_order1[1], ],
                         hap2 = oris1[hap_order1[2], ],
                         pos_recomb = p_recomb[1])
  #same for second hap
  hap_new2 <- recomb_hap(hap1 = haps2[hap_order2[1], ],
                         hap2 = haps2[hap_order2[2], ],
                         pos_recomb = p_recomb[2])
  ori_new2 <- recomb_hap(hap1 = oris2[hap_order2[1], ],
                         hap2 = oris2[hap_order2[2], ],
                         pos_recomb = p_recomb[2])
  list(
    hap1 = hap_new1,
    ori1 = ori_new1,
    hap2 = hap_new2,
    ori2 = ori_new2
  )
}

#' Distribute mutation on a haplotype matrix
#'
#' Produce mutation on a matrix according to a mutation rate
#' @param mat matrix where mutation happen
#' @param mut_rate mutation rate
#' @export
#' @import stats
#'
#' @examples
#' mat <- matrix(0,100,100)
#' mat_mut <- mutate_hap_mat(mat = mat,
#'                           mut_rate = 0.0001)
#' #nb mutation
#' sum(mat_mut)
#'
mutate_hap_mat <- function (mat,
                            mut_rate) {
  #total markers numbers
  mrk_tot <- length(mat)
  #compute mutation number, then take a sample of position
  #according to this number on the whole matrix
  mut.pos <- sample(x = 1:mrk_tot,
                    size = rbinom(n = 1,
                                  size = mrk_tot,
                                  prob = mut_rate))
  #add mutation
  mat[mut.pos] <- !mat[mut.pos]
  return(mat)
}

#' Aggregate haplotype two by two
#'
#' Convert "hap x markers" matrix to "ind x markers" matrix
#' @param mat matrix to convert
#'
#' @return converted matrix
#' @export
#'
#' @examples
#' mat <- matrix(c(0,0,1,0,1,1,1,0,0,1,0,0),ncol=3,byrow = TRUE)
#' print(mat)
#' agg_mat <- aggregate_hap_mat(mat)
#' print(agg_mat)
aggregate_hap_mat <- function (mat) {
  #store row number
  even_pos <- seq(from = 2, to = nrow(mat), by = 2)
  odd_pos <- even_pos - 1
  #add odd and even rows
  agg_mat <- mat[even_pos, ] + mat[odd_pos, ]
  return(agg_mat)
}

#' Detect monomorphic markers
#'
#' Read haplotype matrix and detect monomorphic SNP
#' @param mat haplotype matrix
#' @return vector of monormophic SNP position
#' @export
detect_monomorph <- function (mat) {
  #get count of all alternative allele
  alt_count <- apply(mat, 2, sum)
  max_count <- nrow(mat)
  #get all columns with 0 or max number of alt. allele
  monomorphic <- which(alt_count == 0 | alt_count == max_count)
  print(paste("[INFO] Detected", length(monomorphic), "monomorphic SNP(s)"))
  return(monomorphic)
}

#' Produce an haplotype matrix using scrm (Staab et.al. 2015)
#'
#' Run scrm using coala (Staab et.al. 2016) package, then return a list
#' of haplotype matrix
#' This package is made with 1 recombination per chromosom in mind.
#' So the chromosome size is 1 Morgan (1/[recomb rate] pb)
#' e.g. r = 1e-8, gen_chr_size = 1e8 pb
#' @param n_ind_per_pop number of individuals per population
#' @param t_split_per_pop population split time per population, NULL without split
#' @param N0 /!\ diploid /!\ population size
#' @param mu mutation rate (at one position)
#' @param r recombination rate (between two position)
#' @param chr_size locus length ~ physical chromosome size
#' @param get_model return built model object (for debugging, mainly)
#'
#' @return haplotype matrices list, positions vector
#' @export
#' @import coala stats
generate_hap_matrix_scrm <- function (n_ind_per_pop = rep(100, 3),
                                      t_split_per_pop = c(0.2, 0.1),
                                      N0 = 1e4,
                                      mu = 2.5e-8,
                                      r = 1e-8,
                                      chr_size = 1e8,
                                      get_model = F) {
  #MODEL BUILDING
  gen_chr_size <- 1 / r
  #compute rho and theta according to scrm parametrization
  rho <- 4 * N0 * r * (gen_chr_size - 1)
  theta <- 4 * N0 * mu * gen_chr_size
  #make model
  model <- coal_model (sample_size = n_ind_per_pop,
                       loci_number = 1,
                       loci_length = chr_size)
  model <- model + feat_recombination(rate = rho)
  model <- model + feat_mutation(rate = theta)
  if (! is.null(t_split_per_pop)){
    for (spp in 1:length(t_split_per_pop)) {
      model <- model + feat_pop_merge (time = t_split_per_pop[spp],
                                       pop_source = spp + 1,
                                       pop_target = 1)
    }
  }
  #return model without sumstat
  if (get_model) {
    return(model)
  }

  model <- model + sumstat_seg_sites()
  print(model)

  #RUN SIMULATION
  sim_dump <- simulate(model)

  #OUTPUT SIMULATION
  sim_segsites <- sim_dump$seg_sites[[1]]
  sim_pos <- sim_segsites$position
  sim_hap <- sim_segsites$snps
  #split per population
  sim_hap_list <-
    lapply(split(sim_hap,
                 rep(1:length(n_ind_per_pop),
                     times = n_ind_per_pop)),
           matrix,
           ncol = ncol(sim_hap))
  list(pos = sim_pos,
       hap_list = sim_hap_list)
}
