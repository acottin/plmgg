# Plant like mosaic genome generator (plmgg)

Simulate plant-like admixed individuals to evaluate ancestry inference software. This packages uses coalescence simulator *scrm* and *coala* (Staab et al. 2015, 2016) to produce source populations. Then, a custom forward step is used to breed populations according to configuration files. Then, datasets can be generated in different format from a sample of simulated individuals.

## Example

Best example available right now is the CLI tool `simulation.R` available in my second repository [lai-comparison](https://gitlab.southgreen.fr/acottin/lai-comparison). Configuration files are not versioned and should be generated using the `gen.sh` scripts in the `0-configuration` directory.

## Bibliography

* Staab, P. R., Zhu, S., Metzler, D., & Lunter, G. (2015). scrm: efficiently simulating long sequences using the approximated coalescent with recombination. Bioinformatics, 31(10), 1680–1682. https://doi.org/10.1093/bioinformatics/btu861
* Staab, P. R., & Metzler, D. (2016). Coala: an R framework for coalescent simulation. Bioinformatics, 32(12), 1903–1904. https://doi.org/10.1093/bioinformatics/btw098

